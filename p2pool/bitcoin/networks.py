import os
import platform

from twisted.internet import defer

from . import data
from p2pool.util import math, pack, jsonrpc

def get_subsidy(height):
    halvings = height // 50000
    return (770 * 100000000) >> halvings

nets = dict(
    iridiumcoin=math.Object(
        P2P_PREFIX='fbc0b6db'.decode('hex'),
        P2P_PORT=11060,
        ADDRESS_VERSION=48,
        RPC_PORT=21060,
        RPC_CHECK=defer.inlineCallbacks(lambda bitcoind: defer.returnValue(
            'litecoinaddress' in (yield bitcoind.rpc_help()) and
            not (yield bitcoind.rpc_getinfo())['testnet']
        )),
        SUBSIDY_FUNC=lambda height: get_subsidy(height+1),
        POW_FUNC=lambda data: pack.IntType(256).unpack(__import__('ltc_scrypt').getPoWHash(data)),
        BLOCK_PERIOD=300, # s
        SYMBOL='IRI',
        CONF_FILE_FUNC=lambda: os.path.join(os.path.join(os.environ['APPDATA'], 'Iridiumcoin') if platform.system() == 'Windows' else os.path.expanduser('~/Library/Application Support/Iridiumcoin/') if platform.system() == 'Darwin' else os.path.expanduser('~/.iridiumcoin'), 'iridiumcoin.conf'),
        BLOCK_EXPLORER_URL_PREFIX='',
        ADDRESS_EXPLORER_URL_PREFIX='',
        TX_EXPLORER_URL_PREFIX='',
        SANE_TARGET_RANGE=(2**256//1000000000 - 1, 2**256//1000 - 1),
        DUMB_SCRYPT_DIFF=2**16,
        DUST_THRESHOLD=0.03e8,
    ),
)
for net_name, net in nets.iteritems():
    net.NAME = net_name
