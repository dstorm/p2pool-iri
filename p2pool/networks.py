from p2pool.bitcoin import networks
from p2pool.util import math

# CHAIN_LENGTH = number of shares back client keeps
# REAL_CHAIN_LENGTH = maximum number of shares back client uses to compute payout
# REAL_CHAIN_LENGTH must always be <= CHAIN_LENGTH
# REAL_CHAIN_LENGTH must be changed in sync with all other clients
# changes can be done by changing one, then the other

nets = dict(
    iridiumcoin=math.Object(
        PARENT=networks.nets['iridiumcoin'],
        SHARE_PERIOD=15, # seconds
        CHAIN_LENGTH=24*60*60//15, # shares
        REAL_CHAIN_LENGTH=24*60*60//15, # shares
        TARGET_LOOKBEHIND=60, # shares
        SPREAD=6, # blocks
        IDENTIFIER='e63dc4ef96409e70'.decode('hex'),
        PREFIX='f5a4ebf931e7b76d'.decode('hex'),
        P2P_PORT=10060,
        MIN_TARGET=0,
        MAX_TARGET=2**256//2**20 - 1,
        PERSIST=False,
        WORKER_PORT=10061,
        BOOTSTRAP_ADDRS='iri.altmine.net'.split(' '),
        ANNOUNCE_CHANNEL='#p2pool-iri',
        VERSION_CHECK=lambda v: True,
    ),
)
for net_name, net in nets.iteritems():
    net.NAME = net_name
